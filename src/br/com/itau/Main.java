package br.com.itau;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        Jogador jogador1 = new Jogador("Leonardo");
        List<List<Integer>> grupos = jogador1.sortearGrupos(2, 3);
        int[] gruposSorteados = jogador1.somarGrupos(grupos);
        jogador1.exibirSomaGruposSorteados(gruposSorteados);
    }
}

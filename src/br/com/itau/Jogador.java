package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Jogador {

    private String nome;

    Jogador (String nome) {
        this.nome = nome;
    }

    public List<Integer> jogarDado(int qntdJogadas) {
        List<Integer> numerosSorteados = new ArrayList<>();
        for (int i = 0; i < qntdJogadas; i++) {
            numerosSorteados.add(Dado.sortearNumero());
        }
        return numerosSorteados;
    }

    public List<List<Integer>> sortearGrupos(int qntdGrupos, int qntdJogadas) {
        List<List<Integer>> grupoSorteado = new ArrayList<>();
        for (int i = 0; i < qntdGrupos; i++) {
            grupoSorteado.add(this.jogarDado(qntdJogadas));
        }
        this.exibirGruposSorteados(grupoSorteado);
        return grupoSorteado;
    }

    public int[] somarGrupos(List<List<Integer>> grupos) {
        int somaGrupos[] = new int[grupos.size()];
        int aux = 0;
        for (int i = 0; i < grupos.size(); i++) {
            for (int j = 0; j < grupos.get(i).size(); j++) {
                aux = aux + grupos.get(i).get(j);
            }
            somaGrupos[i] = aux;
            aux = 0;
        }
        return somaGrupos;
    }

    public void exibirGruposSorteados(List<List<Integer>> gruposSorteados) {
        int aux = 1;
        for (int i = 0; i < gruposSorteados.size(); i++) {
            System.out.println("Grupo " + aux + ": ");
            for (int j = 0; j < gruposSorteados.get(i).size(); j++) {
                System.out.print(gruposSorteados.get(i).get(j) + ", ");
            }
            aux++;
            System.out.print("\n");
            System.out.print("\n");
        }
    }

    public void exibirSomaGruposSorteados(int[] somaGrupos) {
        int aux = 1;
        for (int i = 0; i < somaGrupos.length; i++) {
            System.out.print("\nSoma grupo " + aux + ": ");
            System.out.print(somaGrupos[i] + "\n");
            aux++;
        }
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
